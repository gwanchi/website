## Project Title
NextByte ICT Solutions Limited Official Website

## Getting Started

Official website to contain all the products and services offered by NextByte ICT Solutions Limited.

## Prerequisites

- PHP Web Server (Apache/Nginx etc)
- MySQL Database


## Installing

- 1. Copy .env.example to .env (For Linux  cp .env.example .env )
- 2. Install all dependent packages using composer (composer install)
- 3. (Linux Only) Make write permission on the folder storage, bootstrap/cache, .env
- Steps 3
- chmod 777 -R storage
- chmod 777 -R bootstrap/cache
- chmod 777 -R .env

## Running the tests


## Deployment



## Built With

- Laravel - Laravel PHP Framework.
- Composer - PHP Dependency Management.
- PostgreSQL - Database Management System.

## Contributing

Not Applied.

## Versioning

We use Git for versioning. For the versions available, see the tags on this repository.

## Authors

- Erick Chrysostom <gwanchi@gmail.com>.
- Martin Luhanjo <martin.luhanjo@gmail.com>
- Allen Malibate  <allentelesphory@gmail.com>

## License

This project is licensed to NextByte ICT Solutions Limited (Tanzania).

## Copyright

This project work is copyrighted to NextByte ICT Solutions Company Limited (Tanzania).

## Acknowledgments


## Website Template Url

http://localhost/website/public/template/index-2.html